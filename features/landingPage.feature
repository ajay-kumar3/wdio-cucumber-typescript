Feature: Verify Landing page

    Landing page should have valid title

    Background:
        Given I am on "Landing" page

    Scenario: Landing page has proper title
        Then Window title is "WebdriverIO · Next-gen browser and mobile automation test framework for Node.js | WebdriverIO"
